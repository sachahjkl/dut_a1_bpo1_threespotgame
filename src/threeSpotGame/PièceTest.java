package threeSpotGame;

//import static org.junit.Assert.*;

//import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import java.util.Arrays;

public class Pi�ceTest {

	@Test
	public void testPi�ce() {
		Pi�ce p1 = new Pi�ce();
		Pi�ce p2 = new Pi�ce(0, 0, Pi�ce.couleurPi�ce.W);
		assertTrue(Arrays.equals(p1.getCoord1(), p2.getCoord1()));
		assertTrue(Arrays.equals(p1.getCoord2(), p2.getCoord2()));
		assertEquals(p1.getCouleur(), p2.getCouleur());
	}

}
