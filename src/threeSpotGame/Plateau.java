package threeSpotGame;

/**
 * @author FROMENT Sacha, SOLEIMAN Agathe
 */

import java.util.Arrays;
import java.util.HashMap;

import threeSpotGame.Pi�ce.couleurPi�ce;

public class Plateau {
	public static final int SCORE_FIN_JEU = 12, SCORE_CONDITION = 6;
	public static final int SPOT = 3; // la taille du plateau et la colonne
	private int[] points;
	private Pi�ce[] p;
	private HashMap<Integer, Coup> coupsPossibles;
	private int cmpHash;

	/**
	 * @brief initialise le plateau � l'�tat de d�part du jeu.
	 */
	public Plateau() {
		coupsPossibles = new HashMap<Integer, Coup>(4);
		for (int i = 1; i < 5; coupsPossibles.put(i, null), i++);
		p = new Pi�ce[3];
		points = new int[] { 0, 0 };
		p[0] = new Pi�ce(0, 1, couleurPi�ce.R);
		p[1] = new Pi�ce(1, 1, couleurPi�ce.W);
		p[2] = new Pi�ce(2, 1, couleurPi�ce.B);
	}

	/**
	 * @brief Incr�mente de 1 les points d'une pi�ce
	 * @param c couleur de la pi�ce dont on veut incr�menter les points.
	 */
	public void addPoints(couleurPi�ce c) {
		switch (c) {
		case R:
			++points[0];
			break;
		case B:
			++points[1];
			break;
		default:
			break;
		}
	}

	/**
	 * @brief Retourne le nombre de points d'une pi�ce.
	 * @param c couleur de la pi�ce dont on veut r�cup�rer les points.
	 * @return int : le nombre de points de la pi�ce.
	 */
	public int getPoints(couleurPi�ce c) {
		switch (c) {
		case R:
			return points[0];
		case B:
			return points[1];
		default:
			return 0;
		}
	}

	/**
	 * @brief retourne une cha�ne de caract�res repr�sentant l'�tat du plateau de jeu.
	 * @return String : la cha�ne de caract�res.
	 */
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("Etat du plateau :\n\n* * * * * * * * * * * * *\n");
		for (int i = 0; i < SPOT; i++) {
			s.append("*\t*\t*\t*\n*");
			for (int j = 0; j < SPOT; j++) {
				if (estOccup�(i, j) != null) {
					s.append("   " + estOccup�(i, j).getCouleur() + "   *");
					continue;
				}
				if (j == SPOT - 1) {
					s.append("   O   *");
					continue;
				}
				s.append("\t*");
			}
			s.append("\n*\t*\t*\t*\n* * * * * * * * * * * * *\n");
		}
		return s.toString();
	}

	/**
	 * @brief retourne une cha�ne de caract�res repr�sentant les d�placements possibles pour une pi�ce.
	 * @param pi : la pi�ce en train d'�tre jou�e.
	 * @return String : la cha�ne de caract�res.
	 */
	public String toString(Pi�ce pi) {
		int movs = 0;
		StringBuilder s = new StringBuilder();
		s.append("Deplacements possibles :\n\n* * * * * * * * * * * * *\n");
		for (int i = 0; i < SPOT; i++) {
			s.append("*\t*\t*\t*\n*");
			for (int j = 0; j < SPOT; j++) {
				int cmp = 0;
				if (estOccup�(i, j) != null && !estOccup�(i, j).equals(pi)) {
					s.append("   " + estOccup�(i, j).getCouleur() + "   *");
					continue;
				}
				for (int j2 = 1; coupsPossibles.get(j2) != null; ++j2) {
					if (Arrays.equals(coupsPossibles.get(j2).getCoord(), new int[] { i, j })) {
						++movs;
						++cmp;
					}
				}
				if (cmp == 2) {
					s.append("  " + (movs - 1) + "-" + movs + "  *");
					continue;
				}
				if (cmp == 1) {
					s.append("   " + movs + "   *");
					continue;
				}
				if (j == SPOT - 1) {
					s.append("   O   *");
					continue;
				}
				s.append("\t*");
			}
			s.append("\n*\t*\t*\t*\n* * * * * * * * * * * * *\n");
		}
		return s.toString();
	}

	/**
	 * @brief retourne le nombre de mvmts trouv�s pour une pi�ce.
	 * @return int : le nombre de mvmts trouv�s.
	 */
	public int getNbMovs(){ return this.cmpHash;}

	/**
	 * @brief retourne la pi�ce, si il y en a une, qui occupe la case aux coords i,j du plateau.
	 * @param i la ligne du plateau.
	 * @param j la colonne du plateau.
	 * @return Pi�ce : la pi�ce trouv�e.
	 */
	private Pi�ce estOccup�(int i, int j) {
		for (Pi�ce pi : p) {
			if (Arrays.equals(pi.getCoord1(), new int[] { i, j }) || Arrays.equals(pi.getCoord2(), new int[] { i, j }))
				return pi;
		}
		return null;
	}

	private Coup verifCoupV(Pi�ce pi, int i, int j) {
		return verifCoup(pi, i, j, 1, 0, Coup.Direction.V);
	}

	private Coup verifCoupH(Pi�ce pi, int i, int j) {
		return verifCoup(pi, i, j, 0, 1, Coup.Direction.H);
	}

	/**
	 * @param pi
	 * @param i la ligne du plateau.
	 * @param j la colonne du plateau.
	 * @param ip 1 ou 0 si l'ont veut ou non trouver les mouvs verticaux.
	 * @param jp 1 ou 0 si l'ont veut ou non trouver les mouvs horizontaux.
	 * @return le coup trouv�.
	 */
	private Coup verifCoup(Pi�ce pi, int i, int j, int ip, int jp, Coup.Direction d) {
		assert (i >= 0 && j >= 0);
		if (estOccup�(i, j) == null && estOccup�(i - ip, j + jp) == null
				|| estOccup�(i, j) == null && estOccup�(i - ip, j + jp).equals(pi)
				|| estOccup�(i - ip, j + jp) == null && estOccup�(i, j).equals(pi)) {
			return new Coup(i, j, d);
		}
		return null;
	}


	/**
	 * @brief joue le coup donn� pour la pi�ce donn�
	 * @param pi : la pi�ce � jouer.
	 * @param c : le coup � jouer.
	 */
	public void jouer(Pi�ce pi, Coup c) {
		if (c.getDir() == Coup.Direction.V) {
			if (c.getCoord()[1] == SPOT - 1) {
				addPoints(pi.getCouleur());
				addPoints(pi.getCouleur());
			}
			pi.setCoordVertical(c.getCoord()[0], c.getCoord()[1]);
		} else if (c.getDir() == Coup.Direction.H) {
			if (c.getCoord()[1] == SPOT - 2)
				addPoints(pi.getCouleur());
			pi.setCoordHorizontal(c.getCoord()[0], c.getCoord()[1]);
		}
	}

	/**
	 * @param i : le num�ro de la pi�ce voulue (0: R; 1: W; 2: B).
	 * @return la pi�ce trouv�e.
	 */
	public Pi�ce getPi�ce(int i) {
		assert (i >= 0 && i < 3);
		return p[i];
	}

	/**
	 * @brief retourn le coup de cl� i.
	 * @param i : la cl� du coup voulu.
	 * @return le coup associ� � la cl� i.
	 */
	public Coup getCoup(int i) {
		assert (i > 0 && i < 5);
		return coupsPossibles.get(i);
	}

	/**
	 * @brief scanne les coups possibles pour la pi�ce pi sur le plateau.
	 * @param pi : la pi�ce dont on veut scanner les coups.
	 */
	public void scanPlateau(Pi�ce pi) {
		cmpHash = 0;
		for (int i = 0; i < SPOT; i++) {
			for (int j = 0; j < SPOT; j++) {
				if (i > 0 && verifCoupV(pi, i, j) != null) {
					++cmpHash;
					coupsPossibles.put(cmpHash, verifCoupV(pi, i, j));
				}
				if (j < SPOT - 1 && verifCoupH(pi, i, j) != null) {
					++cmpHash;
					coupsPossibles.put(cmpHash, verifCoupH(pi, i, j));
				}
			}
		}
		for (int i = cmpHash + 1; i <= coupsPossibles.size(); ++i) {
			coupsPossibles.put(i, null);
		}
	}
}
